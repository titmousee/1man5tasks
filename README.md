# README #

# Тест 1 

Приложение стартует активити с разметкой, содержащей кнопку и LinearLayout. При нажатии н кнопку в LinearLayout динамически добавляются EditText. Необходимо полностью сохранять состояние добавленных элементов при пересоздании активити (поворот экрана). Для активити не должен использоваться флаг android:configChanges="orientation|screenSize"

# Тест 2

Приложение стартует активити с картой (Google maps api). При тапе на карту появляется TextView с координатами точки нажатия и адресом (получение адреса должно быть однократным и не должно блокировать основной поток приложения). TextView "привязан" к точке нажатия, т.е. при смещении карты TextView перемещается вместе с точкой привязки, при выходе точки за границы видимости, TextView должен оставаться видимым и возобновлять движение вместе с появлением точки привязки в области видимости. При повороте нужно восстановить состояние экрана. Для активити не должен использоваться флаг android:configChanges="orientation|screenSize"

# Тест 3

Возможна ли ситуация в жизненном цикле активити, когда после вызова метода onCreate, метод onDestroy не вызывается?

# Тест 4

Выполнить запрос к одному из погодных сервисов (например, https://openweathermap.org/api) и получить информацию о погоде в формате JSON, основываясь на географических координатах смартфона.

Разобрать JSON и вывести данные о температуре, влажности, давлении, направлении и силе ветра и облачности.

# Тест 5

Необходимо написать приложение для обмена текстовыми сообщениями произвольной длины по протоколу Bluetooth LE. Приложение должно иметь возможность работать в одном из следующих режимов: “Сервер”, “Клиент”. Выбор режима осуществляется при запуске приложения.

Режим “Сервер”

Данный режим предусматривает подключение нескольких клиентов и получение от них сообщений. При получении от какого-либо клиента нового сообщения, остальные клиенты должны иметь возможность получить это сообщение. Необходимо иметь возможность просматривать список сообщений.

Режим “Клиент”

Данный режим предусматривает выбор сервера. После подключения к выбранному серверу необходимо иметь возможность просматривать сообщения, находящиеся на сервере, отправлять сообщения, а так же получать новые сообщения от других клиентов.